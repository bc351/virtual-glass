const dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

let renderGlasses = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    contentHTML += `
   <span  p-2>
    <img class="img" src="${item.virtualImg}" alt=""></img>
    </span>
    `;
  });
  document.getElementById("vglassesList").innerHTML = contentHTML;
};

renderGlasses(dataGlasses);

let chooseGlass = () => {
  let index = 0;
  let allImg = document.querySelectorAll(".img");

  // convert allImg  into an array
  let arrayImg = Array.from(allImg);

  // gan su kien click vao tung img
  arrayImg.forEach((item) => {
    item.addEventListener("click", () => {
      console.log();

      // tim xem index nao duoc click
      // update gia tri cho index
      index = arrayImg.findIndex((i) => {
        return i == item;
      });

      //    sau khi da tim thay index
      // show thong tin
      // su dung vong lap for de xoa het cac hinh
      function removeAllChildNodes(parent) {
        while (parent.firstChild) {
          parent.removeChild(parent.firstChild);
        }
      }
      for (let i = 0; i < arrayImg.length; i++) {
        let avatar = document.getElementById("avatar");
        let imageGlass = arrayImg[index];

        // remove everything

        if (item == arrayImg[index] && avatar.children.length > 0) {
          removeAllChildNodes(avatar);

          let content = "";
          avatar = avatar.appendChild(imageGlass);
          content += `<p>name: ${dataGlasses[index].name}</p>
                                        <span>color: ${dataGlasses[index].color}</span>
                                         <p>price: ${dataGlasses[index].price}</p>
                                         <span>brand: ${dataGlasses[index].brand}</span>
                                         <p>description: ${dataGlasses[index].description}</p>
    `;

          document.getElementById("glassesInfo").style.display = "block";
          document.getElementById("glassesInfo").innerHTML = content;
        } else {
          let content = "";
          avatar = avatar.appendChild(imageGlass);
          content += `<p>name: ${dataGlasses[index].name}</p>
                                        <span>color: ${dataGlasses[index].color}</span>
                                         <p>price: ${dataGlasses[index].price}</p>
                                         <span>brand: ${dataGlasses[index].brand}</span>
                                         <p>description: ${dataGlasses[index].description}</p>
    `;
          document.getElementById("glassesInfo").style.display = "block";
          document.getElementById("glassesInfo").innerHTML = content;
        }
        renderGlasses(dataGlasses);
        chooseGlass();
      }
    });
  });
};

chooseGlass();
